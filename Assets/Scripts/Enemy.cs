﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    public float speed;
    private float dir;
    private void Start()
    {
        if (this.transform.position.x < 0)
            dir = 1;
        else if (this.transform.position.x > 0)
            dir = -1;

        this.transform.localScale = new Vector3(-dir, 1, 1);
        StartCoroutine(Kaboom());
    }

    void Update()
    {
        this.transform.Translate(dir * speed * Time.deltaTime, 0, 0);
    }
    IEnumerator Kaboom()
    {
        yield return new WaitForSeconds(10f);
        Destroy(this.gameObject);
    }
}
