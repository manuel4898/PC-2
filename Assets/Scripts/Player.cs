﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Player : MonoBehaviour
{
    public bool mode = true;

    public float speed;
    private Vector2 playerPos;

    Touch touch;
    private Vector2 initialPosition;

    public Text scoreTxt;
    public Text livesTxt;
    public int score;
    public int lives;

    void Start()
    {
        score = 0;
        lives = 3;
        speed = 300f;
        playerPos = this.transform.position;
        Physics2D.IgnoreLayerCollision(8, 8);
    }

    void Update()
    {
        if (mode) AccMovement();
        else TouchMovement();
        DebugMovement();
        scoreTxt.text = "Score: " + score.ToString();
        livesTxt.text = "Lives: " + lives.ToString();
    }

    private void TouchMovement()
    {
        if (Input.touchCount == 1)
        {
            touch = Input.GetTouch(0);

            if (touch.phase == TouchPhase.Began)
            {
                initialPosition = touch.position;
            }
            else if (touch.phase == TouchPhase.Moved || touch.phase == TouchPhase.Stationary)
            {
                Vector2 direction = touch.position - initialPosition;
                Movement(direction.y);
            }
        }
    }
    private void AccMovement()
    {
        Movement(Input.acceleration.y);
    }

    private void DebugMovement()
    {
        if (Input.GetKey(KeyCode.UpArrow)) Movement(1);
        else if (Input.GetKey(KeyCode.DownArrow)) Movement(-1);
    }
    private void Movement(float dir)
    {
        if (dir > 0)
        {
            playerPos.y += Time.deltaTime * speed;
            if (playerPos.y > 750f + 960) playerPos.y = 750f + 960;
        }
        else if (dir < 0)
        {
            playerPos.y -= Time.deltaTime * speed;
            if (playerPos.y < -750f + 960) playerPos.y = -750f + 960;
        }

        this.transform.position = playerPos;
    }
    public void onClickChangeMode()
    {
        mode = !mode;
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Enemy")
        {
            lives--;
            Destroy(collision.gameObject);
        }
        if (collision.gameObject.tag == "Bonus")
        {
            score++;
            Destroy(collision.gameObject);
        }

    }
}
