﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{
    public float spawnRate;
    public int bonusRate;

    public GameObject[] spawnables;
    [SerializeField] Transform canvas;
    public Player player;
    private Vector2 canvas2D;
    bool isDone = true;

    // Start is called before the first frame update
    void Start()
    {
        canvas2D = new Vector2(canvas.position.x, canvas.position.y);
        spawnRate = Random.Range(1f, 2f);
        foreach (GameObject item in spawnables)
        {
            item.GetComponent<Enemy>().speed = 100f;
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (isDone == true)
        {
            StartCoroutine(FnSpawner());
        }
    }
    IEnumerator FnSpawner()
    {
        isDone = false;
        spawnRate = Random.Range(1f, 2f);
        bonusRate = Random.Range(0, 10);

        float tempY = Random.Range(-650f, 750f);
        float tempX = 620 - 1240 * Random.Range(0, 2);

        Vector2 spawnPosition = new Vector2(tempX, tempY) + canvas2D;

        if (bonusRate > 7)
        {
            spawnables[2].transform.position = spawnPosition;
            Instantiate(spawnables[2]).transform.SetParent(this.transform);
        }
        else
        {
            int id = Random.Range(0, 2);
            spawnables[id].transform.position = spawnPosition;
            Instantiate(spawnables[id]).transform.SetParent(this.transform);
        }

        yield return new WaitForSeconds(spawnRate);

        isDone = true;
    }

}
